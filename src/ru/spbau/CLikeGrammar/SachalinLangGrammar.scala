/**
 * @author Vadim Lomshakov 24/11/13
 *         spbau, 2013
 */
package ru.spbau.CLikeGrammar

import scala.util.parsing.combinator._
import ru.spbau.CLikeGrammar.SachalinLangAst.Literal


/**
 * Sachalin is toy language with C-like syntax and dynamic typing as in Js.
 * It has 4 main types: bigInteger, bigDecimal, stringLiteral, objectLiteral (+ Nil).
 * Control flow is C-like.
 */
object SachalinLangGrammar extends JavaTokenParsers {
  import ru.spbau.CLikeGrammar.SachalinLangAst._

  val intPattern = """(-?\d+)""".r
  // TOKENS
  lazy val keyword =
    "var" | "if" | "else" | "while" | "function" | "return" | "print"

  lazy val id = (not(keyword) ~> ident) ^^ Var

  lazy val number = floatingPointNumber ^^ { case s => s match {     // because OMG parser combinator don't parse int and decimal rules:)
                                             case intPattern(i) => IntLiteral(BigInt(i))
                                             case d => DecimalLiteral(BigDecimal(d))
                                           }}
  lazy val string = stringLiteral ^^ { case s => StringLiteral(s.substring(1, s.size - 1))}

  // EXPRS
  // condition expr
  lazy val bool : Parser[Expression] = chainl1(join, "||" ^^^ LOr)
  lazy val join : Parser[Expression] = chainl1(equality, "&&" ^^^ LAnd)
  lazy val equality : Parser[Expression] =  (rel <~ "==") ~ rel ^^ { case e1 ~ e2 => Eq(e1, e2) } |
                                            (rel <~ "!=") ~ rel ^^ { case e1 ~ e2 => NotEq(e1, e2) } |
                                            rel
  lazy val rel : Parser[Expression] = (expr <~ "<") ~ expr ^^ { case e1 ~ e2 => Lt(e1, e2) } | expr

  // arith expr
  lazy val expr : Parser[Expression] = chainl1(term, "+" ^^^ Add | "-" ^^^ Sub )
  lazy val term : Parser[Expression] = chainl1(unary, "*" ^^^ Mul | "/" ^^^ Div )
  lazy val unary : Parser[Expression] = ref | "-" ~> ref ^^ Minus
  lazy val ref : Parser[Expression] = factor |||
                                      (factor <~ ".") ~ id ^^ { case q ~ i => RefExpr(q, i) } |||
                                      (id <~ "(") ~ repsep(expr, ",") <~ ")" ^^ { case foo ~ params => CallExpr(foo.name, params) }
  lazy val factor : Parser[Expression] = number |
                                         string |
                                         objectLiteral |
                                         id |
                                         "(" ~> expr <~ ")"

  lazy val objectLiteral : Parser[ObjectLiteral] = "{" ~> repsep(property, ",") <~ "}" ^^
    {
      case p: List[(String, Expression)] => ObjectLiteral(collection.mutable.HashMap(p.groupBy(_._1).mapValues(_(0)._2).toSeq : _*))
    }
  lazy val property : Parser[(String, Expression)] = (not(keyword) ~> ident <~ ":") ~ expr ^^ { case name ~ value => (name, value) }

  // STMTS
  lazy val varDeclaration : Parser[Statement] = ("var" ~> id) <~ ";" ^^ { case i => VarDeclaration(i.name) }
  lazy val functionDeclaration : Parser[Statement] = (((("function" ~> id) <~ "(") ~ repsep(id, ",")) <~ ")") ~ blockStmt ^^ 
    { case foo ~ params ~ body => FunctionDeclaration(foo.name, params.map(it => it.name), body) }

  lazy val assignmentStmt : Parser[Statement] = (expr <~ "=") ~ expr <~ ";" ^^ { case lhs ~ rhs => Assignment(lhs, rhs) }
  lazy val exprStmt : Parser[Statement] = expr <~ ";" ^^ { case  e => ExprStmt(e) }
  lazy val printStmt : Parser[Statement] = "print" ~> "(" ~> rep1sep(expr, ",") <~ ")" <~ ";" ^^ { case args => PrintStmt(args) }
  lazy val returnStmt : Parser[Statement] = "return" ~> (expr?) <~ ";" ^^ {
                                                                            case Some(e) => ReturnStmt(e)
                                                                            case None => ReturnStmt(NilLiteral)
                                                                          }
  lazy val whileStmt : Parser[Statement] = (("while" ~> "(" ~> bool) <~ ")") ~ blockStmt ^^ { case cond ~ body => WhileStmt(cond, body) }
  lazy val ifStmt : Parser[Statement] = (("if" ~>  "(" ~> bool) <~ ")") ~ blockStmt ~ (("else" ~> blockStmt )?) ^^ { case cond ~ then ~ ilse => IfStmt(cond, then, ilse) }
  lazy val blockStmt : Parser[List[Statement]] = "{" ~> rep(stmt) <~ "}"

  lazy val stmt : Parser[Statement] = assignmentStmt |
                                       varDeclaration |
                                       ifStmt |
                                       whileStmt |
                                       returnStmt |
                                       printStmt |
                                       exprStmt |
                                       functionDeclaration

  lazy val program : Parser[List[Statement]] = stmt.*

  def dumpAst(input : String) =
    println(parseAll(program, input))
}


object SachalinLangAst {

  trait Expression
  case class LOr(e1: Expression, e2: Expression) extends Expression
  case class LAnd(e1: Expression, e2: Expression) extends Expression
  case class NotEq(e1: Expression, e2: Expression) extends Expression
  case class Eq(e1: Expression, e2: Expression) extends Expression
  case class Lt(e1: Expression, e2: Expression) extends Expression
  case class Add(e1: Expression, e2: Expression) extends Expression
  case class Sub(e1: Expression, e2: Expression) extends Expression
  case class Mul(e1: Expression, e2: Expression) extends Expression
  case class Div(e1: Expression, e2: Expression) extends Expression
  case class Minus(e: Expression) extends Expression
  case class RefExpr(qualifier: Expression, id: Var) extends Expression
  case class CallExpr(foo: String, params: List[Expression]) extends Expression

  case class Var(name: String) extends Expression {
    private var isCalc = false

    def value(context: Context) : Literal = {
      context.resolveVarBy(name) match {
        case Some(binding) => binding match {
          case ObjectLiteral(valObj) if !isCalc => {
            isCalc = true
            val evaluatedObj = ObjectLiteral(collection.mutable.HashMap(valObj.mapValues(Interpreter.evaluate(_)(context)).toSeq : _*))
            context.assignValue(name, evaluatedObj)
            evaluatedObj
          }
          case other => other
        }
        case None => throw new RuntimeException("Unknown identifier: " + name)
      }
    }
  }

  trait Literal extends Expression {
    def integerValue : BigInt
    def decimalValue : BigDecimal
    def stringValue : String
    def toLogic : Boolean
  }

  object Literal {
    implicit def Bool2Literal(value: Boolean) = new FakeBoolLiteral(value)
    implicit def BigInt2Literal(value: BigInt) = new IntLiteral(value)
    implicit def BigDecimal2Literal(value: BigDecimal) = new DecimalLiteral(value)
    implicit def String2Literal(value: String) = new StringLiteral(value)
    implicit def Map2Literal(value: collection.mutable.Map[String, Expression]) = new ObjectLiteral(value)


    def evalEq(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (a : DecimalLiteral, b @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.decimalValue.==(b.decimalValue)
        case (a @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :DecimalLiteral)
        => a.decimalValue.==(b.decimalValue)

        case (a : IntLiteral, b @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.integerValue.==(b.integerValue)
        case (a @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :NilLiteral)
        => a.integerValue.==(b.integerValue)

        case (a : StringLiteral, b @ (_:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.stringValue.==(b.stringValue)
        case (a @ (_:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :StringLiteral)
        => a.stringValue.==(b.stringValue)
      }
    }

    def evalNotEq(vals: (Literal, Literal)) : Literal = {
      !evalEq(vals).toLogic
    }

    def evalLt(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (e1 : DecimalLiteral, e2 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.decimalValue.<(e2.decimalValue)
        case (e1 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :DecimalLiteral)
        => e1.decimalValue.<(e2.decimalValue)

        case (e1 : IntLiteral, e2 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.integerValue.<(e2.integerValue)
        case (e1 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :NilLiteral)
        => e1.integerValue.<(e2.integerValue)
      }
    }

    def evalAdd(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (a : DecimalLiteral, b @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.decimalValue.+(b.decimalValue)
        case (a @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :DecimalLiteral)
        => a.decimalValue.+(b.decimalValue)

        case (a : IntLiteral, b @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.integerValue.+(b.integerValue)
        case (a @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :NilLiteral)
        => a.integerValue.+(b.integerValue)

        case (a : StringLiteral, b @ (_:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => a.stringValue.+(b.stringValue)
        case (a @ (_:StringLiteral | _:ObjectLiteral | _:NilLiteral), b :StringLiteral)
        => a.stringValue.+(b.stringValue)
      }
    }
    
    def evalMul(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (e1 : DecimalLiteral, e2 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.decimalValue.*(e2.decimalValue)
        case (e1 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :DecimalLiteral)
        => e1.decimalValue.*(e2.decimalValue)

        case (e1 : IntLiteral, e2 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.integerValue.*(e2.integerValue)
        case (e1 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :NilLiteral)
        => e1.integerValue.*(e2.integerValue)
      }
    }
    
    def evalSub(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (e1 : DecimalLiteral, e2 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.decimalValue.-(e2.decimalValue)
        case (e1 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :DecimalLiteral)
        => e1.decimalValue.-(e2.decimalValue)

        case (e1 : IntLiteral, e2 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.integerValue.-(e2.integerValue)
        case (e1 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :NilLiteral)
        => e1.integerValue.-(e2.integerValue)
      }
    }
    
    def evalDiv(vals: (Literal, Literal)) : Literal = {
      vals match {
        case (e1 : DecimalLiteral, e2 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.decimalValue./(e2.decimalValue)
        case (e1 @ (_:IntLiteral | _:DecimalLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :DecimalLiteral)
        => e1.decimalValue./(e2.decimalValue)

        case (e1 : IntLiteral, e2 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral))
        => e1.integerValue./(e2.integerValue)
        case (e1 @ (_:IntLiteral | _:StringLiteral | _:ObjectLiteral | _:NilLiteral), e2 :NilLiteral)
        => e1.integerValue./(e2.integerValue)
      }
    }

    def evalNeg(vall: Literal) : Literal = {
      vall match {
        case e : DecimalLiteral => - e.decimalValue
        case e : IntLiteral => - e.integerValue
      }
    }
  }

  trait NilLiteral extends Literal
  case object NilLiteral extends NilLiteral {
    var value = this
    def integerValue = throw new RuntimeException("Nil hasn't conversion to int")
    def decimalValue = throw new RuntimeException("Nil hasn't conversion to decimal")
    def stringValue = "<Nil>"//throw new RuntimeException("Nil hasn't conversion to string")
    def toLogic = false
  }
  case class IntLiteral(value: BigInt) extends Literal {
    def integerValue = value
    def decimalValue = BigDecimal(value)
    def stringValue = value.toString(10)
    def toLogic = if (value == BigInt(0)) false else true
  }
  case class DecimalLiteral(value: BigDecimal) extends Literal {
    def integerValue = value.toBigInt()
    def decimalValue = value
    def stringValue = value.toString()
    def toLogic = if (value == BigDecimal(0.0)) false else true
  }
  case class StringLiteral(value: String)extends Literal {
    def integerValue = try {
                        BigInt(value)
                      } catch {
                        case _: NumberFormatException | _: ArithmeticException
                        => throw new RuntimeException("Can't convert string: " + value + " to decimal")
                      }
    def decimalValue = try {
                        BigDecimal(value)
                      } catch {
                        case _: NumberFormatException | _: ArithmeticException
                        => throw new RuntimeException("Can't convert string: " + value + " to decimal")
                      }
    def stringValue = value
    def toLogic = if (value.isEmpty) false else true
  }
  case class ObjectLiteral(value: collection.mutable.Map[String, Expression]) extends Literal {
    def integerValue = throw new RuntimeException("ObjLiteral hasn't conversion to int")
    def decimalValue = throw new RuntimeException("ObjLiteral hasn't conversion to decimal")
    def stringValue = value.toString()
    def toLogic = true
  }
  case class FakeBoolLiteral(value: Boolean) extends Literal {
    def integerValue = throw new RuntimeException("Bool hasn't conversion to int")
    def decimalValue = throw new RuntimeException("Bool hasn't conversion to decimal")
    def stringValue = throw new RuntimeException("Bool hasn't conversion to string")
    def toLogic = value
  }

  // TODO remove this hack
  case class FakePropertyLiteral(name: String, value: Literal, mapOfParentObj: collection.mutable.Map[String, Expression]) extends Literal {
    def integerValue = throw new RuntimeException("Property hasn't conversion to int")
    def decimalValue = throw new RuntimeException("Property hasn't conversion to decimal")
    def stringValue = value.stringValue
    def toLogic = throw new RuntimeException("Property hasn't conversion to bool")
  }

  trait Statement
  case class VarDeclaration(name: String) extends Statement
  case class FunctionDeclaration(name: String, signature: List[String], body: List[Statement]) extends Statement
  case class Assignment(lhs: Expression, rhs: Expression) extends Statement
  case class ExprStmt(expr: Expression) extends Statement
  case class PrintStmt(args: List[Expression]) extends Statement
  case class ReturnStmt(expr: Expression) extends Statement
  case class WhileStmt(condition: Expression, body: List[Statement]) extends Statement
  case class IfStmt(condition: Expression, then: List[Statement], ilse: Option[List[Statement]]) extends Statement
}


class Interpreter(tree: List[ru.spbau.CLikeGrammar.SachalinLangAst.Statement]) {
  def run() {
    Interpreter.interpret(tree)(EmptyContext)
  }
}

object Interpreter {
  import ru.spbau.CLikeGrammar.SachalinLangAst._

  def interpret(stmts: List[Statement])(implicit context: Context) : Unit = {
    stmts match {

      case VarDeclaration(variable) :: rest => {
        context.declareVar(variable)
        interpret(rest)
      }
      case (foo : FunctionDeclaration) :: rest => {
        context.declareFunction(foo.name, foo)
        interpret(rest)
      }

      case Assignment(lhs, rhs) :: rest => {
        lhs match {
          case Var(name) => context.assignValue(name, evaluate(rhs))
          case FakePropertyLiteral(name, _, parentMap) => parentMap.put(name, evaluate(rhs))
          case _ => throw new RuntimeException("Not " + lhs + " l-value")
        }
        interpret(rest)
      }
      case ExprStmt(expr) :: rest => {
        evaluate(expr)
        interpret(rest)
      }
      case PrintStmt(args) :: rest => {
        args.foreach(it => print(evaluate(it).stringValue))
        interpret(rest)
      }
      case ReturnStmt(expr) :: rest => {
        context.assignValue(Context.returnVal, evaluate(expr))
        context.isReturnFromFunction = true
        // return from function
      }
      case WhileStmt(cond, body) :: rest => {
        while (evaluate(cond).toLogic) {
          interpret(body)
          if (!context.isReturnFromFunction) return // return from function
        }
        interpret(rest)
      }
      case IfStmt(cond, then, ilse) :: rest => {
        if (evaluate(cond).toLogic)
          interpret(then)
        else {
          ilse match {
            case Some(elseBlock) => interpret(elseBlock)
            case None =>
          }
        }
        if (!context.isReturnFromFunction) interpret(rest)
        // return from function
      }
      case Nil =>
      case _ => throw new IllegalArgumentException("unexpected ast node")
    }
  }

  def evaluate(expr: Expression)(implicit context: Context) : Literal = {
    import Literal._
    expr match {
      case LOr(e1 , e2) => evaluate(e1).toLogic.||(evaluate(e2).toLogic)
      case LAnd(e1, e2) => evaluate(e1).toLogic.&&(evaluate(e2).toLogic)

      case Eq(e1, e2) => evalEq(evaluate(e1), evaluate(e2))
      case NotEq(e1, e2) => evalNotEq(evaluate(e1), evaluate(e2).toLogic)
      case Lt(e1, e2) => evalLt(evaluate(e1), evaluate(e2).toLogic)

      case Add(e1, e2) => evalAdd(evaluate(e1), evaluate(e2))
      case Sub(e1, e2) => evalSub(evaluate(e1), evaluate(e2)) 
      case Mul(e1, e2) => evalMul(evaluate(e1), evaluate(e2))
      case Div(e1, e2) => evalDiv(evaluate(e1), evaluate(e2))
      case Minus(e) => evalNeg(evaluate(e))

      case variable: Var => variable.value(context)
      case value : NilLiteral => value
      case value : IntLiteral => value
      case value : DecimalLiteral => value
      case value : StringLiteral => value
      case value : ObjectLiteral => value
      case RefExpr(qualifier, identifier) => {
        evaluate(qualifier) match {
          case ObjectLiteral(mapValues) => FakePropertyLiteral(identifier.name , evaluate(mapValues(identifier.name)), mapValues)
          case _ => throw new RuntimeException(f"Expression: $expr%s hasn't property $identifier%s")
        }
      }
      case FakePropertyLiteral(_, value, _) => value

      case CallExpr(fooName, params) => {
        val foo = context.resolveFunctionBy(fooName) match {
          case Some(vall) => vall
          case None => throw new RuntimeException("Function " + fooName + " not found into this scope")
        }
        val childContext = context.child
        if (foo.signature.size != params.size) throw new RuntimeException("Call expr isn't satisfy signature")
        // pass args
        foo.signature.zip(params).foreach {case (name, value) => childContext.declareVar(name, evaluate(value))}
        interpret(foo.body)(childContext)
        childContext.resolveVarBy(Context.returnVal).get
      }
      case _ => throw new IllegalArgumentException("unexpected type or expression")
    }
  }
}



/**
 * notes@ - 1 Block scope doesn't hide declaration of variables for simple implementation.
 *        - 2 overriding function isn't supported, but its adding is simple
 *        - 3 mutual recursion isn't implemented, need to replace list of context on mapping function to context
 */
class Context(vars: collection.mutable.Map[String, Literal], funcs: collection.mutable.Map[String, ru.spbau.CLikeGrammar.SachalinLangAst.FunctionDeclaration], parent: Option[Context]) {
  import ru.spbau.CLikeGrammar.SachalinLangAst._
  // init return register
  vars.put(Context.returnVal, NilLiteral)

  var isReturnFromFunction = false
  lazy val child = new Context(new collection.mutable.HashMap[String, Literal](), new collection.mutable.HashMap[String, FunctionDeclaration](), Some(this))


  def declareVar(binding: String, value: Literal = NilLiteral) : Unit = {
    vars.get(binding) match {
      case Some(_) => throw new RuntimeException("Variable " + binding + "is declared twice")
      case None =>
    }
    vars.put(binding, value)
  }

  def assignValue(binding: String, value: Literal = NilLiteral) : Unit = {
    resolveVarBy(binding, recurs = false) match {
      case Some(_) => vars.put(binding, value)
      case None => throw new RuntimeException("Var " + binding + " is undeclared")
    }
  }

  def declareFunction(binding: String, value: FunctionDeclaration) : Unit = {
    funcs.put(binding, value)
  }

  def resolveVarBy(name: String, recurs: Boolean = true) : Option[Literal] = {
    if (vars contains name) {
      Some(vars(name))
    } else {
      parent match {
        case Some(c) if recurs => c resolveVarBy name
        case None => None
      }
    }
  }

  def resolveFunctionBy(name: String) : Option[FunctionDeclaration] = {
    if (funcs contains name) {
      Some(funcs(name))
    } else {
      parent match {
        case Some(c) => c resolveFunctionBy name
        case None => None
      }
    }
  }

}

object Context {
  val returnVal = "return"
}

object EmptyContext extends Context(new collection.mutable.HashMap[String, Literal](),
  new collection.mutable.HashMap[String, ru.spbau.CLikeGrammar.SachalinLangAst.FunctionDeclaration](), None)