import ru.spbau.CLikeGrammar.{EmptyContext, Interpreter, SachalinLangGrammar}

/**
 * @author Vadim Lomshakov 24/11/13
 *         spbau, 2013
 */
object main extends App{

  SachalinLangGrammar.parseAll(SachalinLangGrammar.program,
    "function fib(x) { " +
      " if (x == 0 || x == 1) {" +
      "   return 1;" +
      " } " +
      " return x * fib(x - 1);" +
      "}" +
      "print(\"fib #3 = \", fib(10), {a: \"Hello world! version:\"}.a, 10.0 + \"100\", \"a\");"
  ) match {
    case ast if !ast.isEmpty => new Interpreter(ast.get).run()
    case ast => println(ast)
  }



}
